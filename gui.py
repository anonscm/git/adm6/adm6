# -*- utf8
#
import wx
import wx.lib.mixins.listctrl as listmix
#from ipaddr import IPv6Network
from UserDict import UserDict

# initially derived from an example program named notepad-example.py
#
""" gui.py is main gui application for ipv6 packetfilter generator
"""


class HN6Dict(UserDict):
    """Object contains a dict with content like this:
        hn6["name"] = [[IPv6Address as string, comment as string],]"""

    def __init__(self, Dict=None, **kwargs):
        """initialize hn6-Object as UserDict"""
        UserDict.__init__(self, Dict, **kwargs)
        self.read_hostnet6_file()

    def preset(self):
        self[100] = ("many", "2000::/3", "# every possible Address")
        self[200] = ("mail", "2001:db8:f02:1::25/128", "# mailserver")
        self[300] = ("ns", "2001:db8:f02:1::23/128", "# nameserver")
        self[400] = ("www", "2001:db8:f02:1::80/128", "# webserver")
        self[500] = ("admin", "2001:db8:f02:2::5:23/128", "# admin4all")
        self[600] = ("router-isp", "2001:db8:f02::1/128", "# ISP")
        self[700] = ("router-mine", "2001:db8:f02::2/128", "# to ISP")
        self[800] = ("my-net", "2001:db8:f02::/48", "# my PA")
        pass

    def __repr__(self):
        """perhaps to print it"""
        for k in self.keys():
            entries = self[k]
            #for a in entries:
            #    print "%-20s %-62s" % (k, a )
        return ''

    def read_hostnet6_file(self, filename="/home/hans/adm6/etc/hostnet6"):
        """read hostnet6-file into a list of 3 element-lists,
            every line containing: name, address, comment
            address need to be a valid IPv6Network-address
            """
        #self = {}
        file1 = open(filename, 'r')
        linenr = 0
        for zeile in file1:
            linenr = linenr + 1
            line = str(zeile)
            lefthalf = line.split('#')
            try:
                (name, addr) = lefthalf.pop(0).split()
                (comment) = lefthalf.pop(0).split()
                comm = "#"
                while len(comment):
                    comm = comm + " " + comment.pop(0)
                self[str(linenr)] = ( name, addr, comm )
            except:
                pass
        file1.close()


class HN6ListCtrlmix(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin,
            listmix.ColumnSorterMixin):

    def __init__(self, parent, log):
        wx.ListCtrl.__init__(self, parent, -1,
            style=wx.LC_REPORT|wx.LC_VIRTUAL|wx.LC_HRULES|wx.LC_VRULES)
        self.log=log
        #adding some art
        self.il = wx.ImageList(16, 16)
        a = {"sm_up": "GO_UP", "sm_dn": "GO_DOWN", }
        for k, v in a.items():
            s = "self.%s=self.il.Add(wx.ArtProvider_GetBitmap(wx.ART_%s,wx.ART_TOOLBAR, (16, 16)))" % (k, v)
            exec(s)
        self.SetImageList(self.il, wx.IMAGE_LIST_SMALL)
#        #adding some attributes (colourful background for each item rows)
        #self.attr1 = wx.ListItemAttr()
#        self.attr1.SetBackgroundColour("yellow")
        self.attr2 = wx.ListItemAttr()
#        self.attr2.SetBackgroundColour("light blue")
        self.attr3 = wx.ListItemAttr()
#        self.attr3.SetBackgroundColour("purple")
        #building the columns
        self.InsertColumn(0, "Name")
        self.InsertColumn(1, "Adress")
        self.InsertColumn(2, "# Comment")
        self.SetColumnWidth(0, 150)
        self.SetColumnWidth(1, 320)
        self.SetColumnWidth(2, 100)
        #These two should probably be passed to init more cleanly
        #setting the numbers of items = number of elements in the dictionary
        hostnet6Dict = HN6Dict()
        hostnet6Dict.read_hostnet6_file()
        self.itemDataMap = hostnet6Dict
        self.itemIndexMap = hostnet6Dict.keys()
        self.SetItemCount(len(hostnet6Dict))
        #mixins
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        listmix.ColumnSorterMixin.__init__(self, 3)
        #self.SortListItems(2, 1)
        #self.SortListItems(0, 1) # col 0 (it is the frst one) direction abc=1
        #events
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnItemSelected)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnItemActivated)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.OnItemDeselected)
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColClick)

    def OnColClick(self, event):
        event.Skip()

    def OnItemSelected(self, event):
        self.currentItem = event.m_itemIndex
        self.log.WriteText('OnItemSelected: "%s", "%s", "%s", "%s"\n' %
                           (self.currentItem,
                            self.GetItemText(self.currentItem),
                            self.getColumnText(self.currentItem, 1),
                            self.getColumnText(self.currentItem, 2)))

    def OnItemActivated(self, event):
        self.currentItem = event.m_itemIndex
        self.log.WriteText("OnItemActivated: %s\nTopItem: %s\n" %
                           (self.GetItemText(self.currentItem),
                            self.GetTopItem()))

    def getColumnText(self, index, col):
        item = self.GetItem(index, col)
        return item.GetText()

    def OnItemDeselected(self, evt):
        self.log.WriteText("OnItemDeselected: %s" % evt.m_itemIndex)

    def SortItems(self, sorter=cmp):
        items = list(self.itemDataMap.keys())
        items.sort(sorter)
        self.itemIndexMap = items
        # redraw the list
        self.Refresh()

    #---------------------------------------------------
    # These methods are callbacks for implementing the
    # "virtualness" of the list...
    def OnGetItemText(self, item, col):
        index=self.itemIndexMap[item]
        s = self.itemDataMap[index][col]
        return s

    def OnGetItemAttr(self, item):
        index=self.itemIndexMap[item]
        genre=self.itemDataMap[index][2]

        if genre=="Rock":
            return self.attr2
        elif genre=="Jazz":
            return self.attr1
        elif genre=="New Age":
            return self.attr3
        else:
            return None


#    def OnGetItemImage(self, item):
#        index=self.itemIndexMap[item]
#        genre=self.itemDataMap[index][2]
#
#        if genre=="Rock":
#            return self.w_idx
#        elif genre=="Jazz":
#            return self.e_idx
#        elif genre=="New Age":
#            return self.i_idx
#        else:
#            return -1

    # Used by the ColumnSorterMixin, see wx/lib/mixins/listctrl.py
    def GetListCtrl(self):
        return self

    # Used by the ColumnSorterMixin, see wx/lib/mixins/listctrl.py
    def GetSortImages(self):
        return (self.sm_dn, self.sm_up)


class Log:
    r"""\brief Needed by the wxdemos.
    The log output is redirected to the status bar of the containing frame.
    """

    def WriteText(self, text_string):
        self.write(text_string)

    def write(self, text_string):
        wx.GetApp().GetTopWindow().SetStatusText(text_string)


#class HostNetEditor(wx.Frame, listmix.ColumnSorterMixin):
#    """graphical editor for hostnet6 file, listbox with: close-,
#    add-, del-, sort_by_name-, sort_by_addr- button"""
#
#    def __init__(self, parent, id):
#        self.entries = []
#        self.read_hostnet6_file()
#        index = -1
#        #wx.MiniFrame.__init__(self, parent,
#        wx.Frame.__init__(self, parent,
#            id, 'hostnet6 editor', pos=(100, 100), size=(850, 300))
#        panel = wx.Panel(self)
#        btn_Cls = wx.Button(panel, label="&Close", size=(50, 30), pos=(0, 20))
#        btn_Add = wx.Button(panel, label="&Add", size=(50, 30), pos=(0, 70))
#        btn_Chg = wx.Button(panel, label="Ch&g", size=(50, 30), pos=(0, 120))
#        btn_Del = wx.Button(panel, label="&Del", size=(50, 30), pos=(0, 170))
#        self.Bind(wx.EVT_CLOSE, self.OnCloseWindow)
#        il = wx.ImageList(16, 16, True)
#        self.up = il.AddWithColourMask(wx.Bitmap("sm_up.bmp",
#                wx.BITMAP_TYPE_BMP), "blue")
#        self.dn = il.AddWithColourMask(wx.Bitmap("sm_down.bmp",
#                wx.BITMAP_TYPE_BMP), "blue")
#        self.lbox = wx.ListCtrl(panel, -1,
#                    style=wx.LC_REPORT,# | wx.LC_EDIT_LABELS,
#                    size=(740, 250), pos=(90, 20))
#        self.lbox.AssignImageList(il, wx.IMAGE_LIST_SMALL)
#        self.lbox.InsertColumn(-1, 'Name   ', format=wx.LIST_FORMAT_LEFT)
#        self.lbox.InsertColumn(-1, 'Address', format=wx.LIST_FORMAT_LEFT)
#        self.lbox.InsertColumn(-1, '# Comment', format=wx.LIST_FORMAT_LEFT)
#        self.lbox.SetColumnWidth(0, 120)
#        self.lbox.SetColumnWidth(1, 330)
#        self.lbox.SetColumnWidth(2, 270)
#        items = self.entries
#        self.itemDataMap = {}
#        for entry in items:
#            index = index + 1
#            (name, address, comment) = entry
#            #print name, address, comment
#            self.lbox.InsertStringItem(index, name)
#            self.lbox.SetStringItem(index, 0, name)
#            self.lbox.SetStringItem(index, 1, address)
#            self.lbox.SetStringItem(index, 2, comment)
#            self.lbox.SetItemData(index, index)
#            self.itemDataMap[index] = entry
#        listmix.ColumnSorterMixin.__init__(self, 3)
#        self.Bind(wx.EVT_BUTTON, self.OnCloseMe, btn_Cls)
#        self.Bind(wx.EVT_BUTTON, self.OnAdd, btn_Add)
#        self.Bind(wx.EVT_BUTTON, self.OnChg, btn_Chg)
#        self.Bind(wx.EVT_BUTTON, self.OnDel, btn_Del)
#
#    def GetListCtrl(self):
#        return self.lbox
#
#    def GetSortImages(self):
#        return (self.dn, self.up)
#
#    def OnCloseWindow(self, event):
#        self.Destroy()
#
#    def OnCloseMe(self, event):
#        print "close button pressed, finished"
#        self.Close(False)
#
#    def OnAdd(self, event):
#        print "add button pressed"
#
#    def OnChg(self, event):
#        print "chg button pressed"
#
#    def OnDel(self, event):
#        print "del button pressed"
#
#    def read_hostnet6_file(self, filename="/home/hans/adm6/etc/hostnet6"):
#        """read hostnet6-file into a list of 3 element-lists,
#            every line containing: name, address, comment
#            address need to be a valid IPv6Network-address
#            """
#        self.entries = []
#        file1 = open(filename, 'r')
#        linenr = 0
#        for zeile in file1:
#            linenr = linenr + 1
#            line = str(zeile)
#            lefthalf = line.split('#')
#            try:
#                (name, addr) = lefthalf.pop(0).split()
#                (comment) = lefthalf.pop(0).split()
#                comm = "#"
#                while len(comment):
#                    comm = comm + " " + comment.pop(0)
#                try:
#                    ipad = IPv6Network(addr)
#                    if self.entries.count([name, str(ipad), comm]) == 0:
#                        #print "N:",name,"A:",str(ipad),"C:",comm
#                        self.entries.append([name, str(ipad), comm])
#                except:
#                    #print "User-Error: file:", filename
#                    print "User-Error: line:", linenr
#                    print "User-Error:", file, "Line:", linenr,
#                    print "Content:", zeile
#                    pass
#                finally:
#                    pass
#            except:
#                pass
#        file1.close()


class RuleDict(UserDict):
    """Object contains a dict with content like this:
        rd["name"] = [[num,src,dst,prto,port,act,opt,comm],]"""

    def __init__(self, Dict=None, **kwargs):
        """initialize hn6-Object as UserDict"""
        UserDict.__init__(self, Dict, **kwargs)

    def preset(self):
        self[1] = \
    ("1", "many", "ns", "udp", "53", "accept", "", "# any dns-requests")
        self[2] = \
    ("2", "ns", "many", "udp", "53", "accept", "NOSTATE", "# ns dns-requests")
        self[3] = \
    ("3", "admin", "ns", "tcp", "22", "accept", "", "# administration")
        self[4] = \
    ("4", "ns", "r-ex", "tcp", "22", "accept", "NOIF", "# administration")
        self[5] = \
    ("5", "admin", "many", "udp", "53", "accept", "NOSTATE",
            "# admins dns-requests")

    def __repr__(self):
        """perhaps to print it"""
        for k in self.keys():
            entries = self[k]
            for a in entries:
                print "%-20s %-62s" % (k, a)
        return ''


class RuleListCtrlmix(wx.ListCtrl, listmix.ListCtrlAutoWidthMixin,
            listmix.ColumnSorterMixin):

    def __init__(self, parent, log):
        wx.ListCtrl.__init__(self, parent, -1,
            style=wx.LC_REPORT|wx.LC_VIRTUAL|wx.LC_HRULES|wx.LC_VRULES)
        self.log=log
        #adding some art
        self.il = wx.ImageList(16, 16)
        a={"sm_up": "GO_UP", "sm_dn": "GO_DOWN"}
        for k, v in a.items():
            s = \
    "self.%s=self.il.Add(wx.ArtProvider_GetBitmap(wx.ART_%s,wx.ART_TOOLBAR,(16,16)))" % (k, v)
            exec(s)
        self.SetImageList(self.il, wx.IMAGE_LIST_SMALL)
#        #adding some attributes (colourful background for each item rows)
        #self.attr1 = wx.ListItemAttr()
#        self.attr1.SetBackgroundColour("yellow")
        self.attr2 = wx.ListItemAttr()
#        self.attr2.SetBackgroundColour("light blue")
        self.attr3 = wx.ListItemAttr()
#        self.attr3.SetBackgroundColour("purple")
        #building the columns
        self.InsertColumn(0, "Num")
        self.InsertColumn(1, "Source")
        self.InsertColumn(2, "Destin")
        self.InsertColumn(3, "Proto")
        self.InsertColumn(4, "Port")
        self.InsertColumn(5, "Action")
        self.InsertColumn(6, "Option")
        self.InsertColumn(7, "#Comment")
        self.SetColumnWidth(0, 40)
        #self.SetColumnWidth(1, 30)
        #self.SetColumnWidth(2, 30)
        #These two should probably be passed to init more cleanly
        #setting the numbers of items = number of elements in the dictionary
        ruleDict = RuleDict()
        ruleDict.preset()
        #hostnet6Dict[1] = ("m6",
        #                "2001:0db8:abba:beef:dada:daad:dada:daad/128",
        #                "# Comment for m6")
        #hostnet6Dict[3] = ("adm6", "::/0", "# Comment for adm6")
        #hostnet6Dict[5] = ("r-ex", "::/0", "# Comment for r-ex")
        self.itemDataMap = ruleDict
        self.itemIndexMap = ruleDict.keys()
        self.SetItemCount(len(ruleDict))
        #mixins
        listmix.ListCtrlAutoWidthMixin.__init__(self)
        listmix.ColumnSorterMixin.__init__(self, 8)
        #self.SortListItems(2, 1)
        #self.SortListItems(0, 1) # col 0 (it is the frst one) direction abc=1
        #events
        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.OnItemSelected)
        self.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.OnItemActivated)
        self.Bind(wx.EVT_LIST_ITEM_DESELECTED, self.OnItemDeselected)
        self.Bind(wx.EVT_LIST_COL_CLICK, self.OnColClick)

    def OnColClick(self, event):
        event.Skip()

    def OnItemSelected(self, event):
        self.currentItem = event.m_itemIndex
        self.log.WriteText('OnItemSelected: "%s", "%s", "%s", "%s"\n' %
                           (self.currentItem,
                            self.GetItemText(self.currentItem),
                            self.getColumnText(self.currentItem, 1),
                            self.getColumnText(self.currentItem, 2)))

    def OnItemActivated(self, event):
        self.currentItem = event.m_itemIndex
        self.log.WriteText("OnItemActivated: %s\nTopItem: %s\n" %
                           (self.GetItemText(self.currentItem),
                            self.GetTopItem()))

    def getColumnText(self, index, col):
        item = self.GetItem(index, col)
        return item.GetText()

    def OnItemDeselected(self, evt):
        self.log.WriteText("OnItemDeselected: %s" % evt.m_itemIndex)

    def SortItems(self, sorter= cmp):
        items = list(self.itemDataMap.keys())
        items.sort(sorter)
        self.itemIndexMap = items

        # redraw the list
        self.Refresh()

    #---------------------------------------------------
    # These methods are callbacks for implementing the
    # "virtualness" of the list...
    def OnGetItemText(self, item, col):
        index=self.itemIndexMap[item]
        s = self.itemDataMap[index][col]
        return s

    def OnGetItemAttr(self, item):
        index=self.itemIndexMap[item]
        genre=self.itemDataMap[index][2]

        if genre=="Rock":
            return self.attr2
        elif genre=="Jazz":
            return self.attr1
        elif genre=="New Age":
            return self.attr3
        else:
            return None


#    def OnGetItemImage(self, item):
#        index=self.itemIndexMap[item]
#        genre=self.itemDataMap[index][2]
#
#        if genre=="Rock":
#            return self.w_idx
#        elif genre=="Jazz":
#            return self.e_idx
#        elif genre=="New Age":
#            return self.i_idx
#        else:
#            return -1

    # Used by the ColumnSorterMixin, see wx/lib/mixins/listctrl.py
    def GetListCtrl(self):
        return self

    # Used by the ColumnSorterMixin, see wx/lib/mixins/listctrl.py
    def GetSortImages(self):
        return (self.sm_dn, self.sm_up)


class PageStatus(wx.Panel):
    """no second chance for a first impression: gui comes up with this page"""

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)
        #t = wx.StaticText(self, -1, "Status", (20, 20))

class PageDevices(wx.Panel):
    """no second chance for a first impression: gui comes up with this page"""

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)


class PageHostNet6(wx.Panel):
    """ hostnet6 definitions editor"""

    def __init__(self, parent, log):
        wx.Panel.__init__(self, parent)
        self.win = HN6ListCtrlmix(self, log)
        self.win.Position = (10, 10)
        self.win.Size = (780, 400)
        btn_Add = wx.Button(self, label="&Add def",
                        size=(90, 30), pos=(10, 420))
        btn_Del = wx.Button(self, label="D&el def",
                        size=(90, 30), pos=(110, 420))
        btn_Chg = wx.Button(self, label="Ch&g def",
                        size=(90, 30), pos=(210, 420))
        self.Bind(wx.EVT_BUTTON, self.OnAdd, btn_Add)
        self.Bind(wx.EVT_BUTTON, self.OnChg, btn_Chg)
        self.Bind(wx.EVT_BUTTON, self.OnDel, btn_Del)
        btn_Del.Enabled = False
        btn_Chg.Enabled = False

    def OnAdd(self, event):
        print "add button pressed"

    def OnChg(self, event):
        print "chg button pressed"

    def OnDel(self, event):
        print "del button pressed"


class PageRules(wx.Panel):
    """ rules editor"""

    def __init__(self, parent, log):
        wx.Panel.__init__(self, parent)
        self.win = RuleListCtrlmix(self, log)
        self.win.Position = (10, 10)
        self.win.Size = (780, 400)
        btn_Add = wx.Button(self, label="&Add rule",
                                size=(90, 30), pos=(10, 420))
        btn_Del = wx.Button(self, label="D&el rule",
                                size=(90, 30), pos=(110, 420))
        btn_Chg = wx.Button(self, label="Ch&g rule",
                                size=(90, 30), pos=(210, 420))
        btn_Up = wx.Button(self, label="&Up",
                                size=(90, 30), pos=(310, 420))
        btn_Dn = wx.Button(self, label="&Down",
                                size=(90, 30), pos=(410, 420))
        self.Bind(wx.EVT_BUTTON, self.OnAdd, btn_Add)
        self.Bind(wx.EVT_BUTTON, self.OnChg, btn_Chg)
        self.Bind(wx.EVT_BUTTON, self.OnDel, btn_Del)
        self.Bind(wx.EVT_BUTTON, self.OnUp, btn_Up)
        self.Bind(wx.EVT_BUTTON, self.OnDn, btn_Dn)
        btn_Chg.Enabled = False
        btn_Del.Enabled = False
        btn_Up.Enabled = False
        btn_Dn.Enabled = False

    def OnAdd(self, event):
        print "add button pressed"

    def OnChg(self, event):
        print "chg button pressed"

    def OnDel(self, event):
        print "del button pressed"

    def OnUp(self, event):
        print "Up"

    def OnDn(self, event):
        print "Down"


class PageApply(wx.Panel):
    """apply the rules, build all the shell scripts"""

    def __init__(self, parent):
        wx.Panel.__init__(self, parent)


#class PageFive(wx.Panel):
#    def __init__(self, parent):
#        wx.Panel.__init__(self, parent)
#        t = wx.StaticText(self, -1, "This is a PageFive object", (20,20))

class MainFrame(wx.Frame):
    """ gui application class, runs only once!"""

    def __init__(self):
        #filename = "/home/hans/adm6/etc/hostnet6"
        wx.Frame.__init__(self, None,
                            title="adm6 - IPv6 packetfilter generator",
                            size=(850, 600))
        # Here we create a panel and a notebook on the panel
        btn_EXIT = wx.Button(self, label="E&xit", pos=(20, 10), size=(80, 25))
        self.Bind(wx.EVT_BUTTON, self.OnCloseMain, btn_EXIT)
        p = wx.Panel(self, pos=(20, 50), size=(810, 500))
        nb = wx.Notebook(p)
        self.CreateStatusBar()
        self.log = Log()
        self.log.WriteText('application started')
        #p = wx.Panel(self, pos=(20,50), size=(810, 500))
        #nb = wx.Notebook(p)
        # create the page windows as children of the notebook
        page0 = PageStatus(nb)
        page1 = PageDevices(nb)
        page2 = PageHostNet6(nb, self.log)
        page3 = PageRules(nb, self.log)
        page4 = PageApply(nb)

        # add the pages to the notebook with the label to show on the tab
        nb.AddPage(page0, "Status")
        nb.AddPage(page1, "Devices")
        nb.AddPage(page2, "Definitions")
        nb.AddPage(page3, "Rules")
        nb.AddPage(page4, "Apply")
        #nb.AddPage(page5, "Page 5")

        # finally, put the notebook in a sizer for the panel to manage
        # the layout
        sizer = wx.BoxSizer()
        sizer.Add(nb, 1, wx.EXPAND)
        p.SetSizer(sizer)

    def OnCloseMain(self, event):
        print "Exit button pressed, finished"
        self.Close(False)


if __name__ == "__main__":
    app = wx.App()
    MainFrame().Show()
    app.MainLoop()
