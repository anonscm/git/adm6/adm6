#! /usr/bin/env python
#coding=utf-8
""" Module provides object for ssh-host-communication within adm6:

    Main purposes:
    1. get interface and routing informations from remote hosts
    2. distribute generated filters
    3. and execute them there: apply
"""

import socket
import paramiko
import sys
import subprocess
from adm6configparser import Adm6ConfigParser
from adm6git import Adm6Git


class RemoteHost():
    """ RemoteHost object provides neccessary stuff to communicate eith adm6:
        Methods are higly OS-dependant, so we need to read config first"""

    def __init__(self, name):
        """initialize RemoteHost Object with given Name"""
        paramiko.util.log_to_file('ssh-sessions.log', 30)
        self.config = Adm6ConfigParser()
        self.name = name
        self.host = name
        self.adm6git = Adm6Git()
        self.adm6git.commit("Initial commit")
        #host_section = u"device#" + name
        self.oss = self.config.get_os(name)
        self.active = False
        self.destport = 22
        self.username = 'root'
        self.session_timeout = 10
        self.priv_key = self.config.get_key_filename()
        self.key = paramiko.RSAKey.from_private_key_file(self.priv_key)
        try:
            self.system = self.config.get_os(name)
            self.path = self.config.get_device_home(name)
            self.active = self.config.get_apply(name)
        except KeyboardInterrupt, err:
            print "User Interrupt, try to continue," + err.strerror
        except:
            print "\nError in config -", sys.exc_info()[1]
        if self.active:
            self.adm6git = Adm6Git()
            self.adm6git.commit("Initial commit on the fly:" + self.name)
            print "RemoteHost initialized: ", name
        else:
            print "RemoteHost is inactive: ", name

    def _copy_file_to_remote(self, local_file, remote_file):
        """copy local file to given remote path"""
        rem = "root@%s:%s" % (self.host, remote_file)
        cmd = "/usr/bin/scp -q "
        cmd = cmd + local_file + " "
        cmd = cmd + rem
        ret = subprocess.call(cmd, shell=True)
        return ret

    def _exec_remote_cmd(self, rem_cmd):
        """return remote executed commands output"""
        retstr = ""
        reterr = ""
        if self.active:
            ses = paramiko.SSHClient()
            ses.load_system_host_keys()
            try:
                ses.connect(self.host, self.destport, self.username,
                          timeout=self.session_timeout, pkey=self.key)
                stdin, stdout, stderr = ses.exec_command(rem_cmd)
                stdin.close()
                retstr = stdout.read()
                reterr = stderr.read()
                ses.close()
            except socket.error, err:
                retstr = "Error: timeout or connection refused:" + self.host
                reterr = err.strerror
                print "_exec_remote_cmd:", reterr,
        return retstr, reterr

    def _read_remote_command(self, command): #, file_to_write):
        """return remote executed commands output"""
        remote_output = ""
        remote_error = ""
        if self.active:
            ses = paramiko.SSHClient()
            ses.load_system_host_keys()
            try:
                ses.connect(self.host,
                          self.destport,
                          self.username,
                          timeout=self.session_timeout,
                          pkey=self.key)
                stdin, stdout, stderr = ses.exec_command(command)
                stdin.close()
                remote_output = stdout.read()
                remote_error = stderr.read()
                ses.close()
            except socket.error, err:
                print "Error: timeout or connection refused: :", self.host
                remote_error = err.strerror
        return remote_output, remote_error

    def _write_local_file(self, filename, content):
        """write content to filename or exit with error"""
        fullname = self.path + "/" + filename + ".new"
        try:
            localfile = open(fullname, 'w')
            localfile.write(content)
            localfile.close()
        except IOError, err:
            retstr = "Write failed on file: " + fullname + err.strerror
            retstr = retstr + "\nAbort!"
            print retstr
            sys.exit()

    def apply_newest_output(self):
        """copy local filters to remote and execute them"""
        newest = This.path + "/output"
        destin = "/root/out-new"
        ret = self._copy_file_to_remote(newest, destin)
        if ret:
            print "copying failed to: %s" % (self.name)
            sys.exit(1)
        cmd = "/bin/bash /root/out-new"
        ret, err = self._exec_remote_cmd(cmd)
        if len(err) > 0:
            print "ERROR:", err
        self.adm6git.add(self.path)
        self.adm6git.commit("apply made output: " + self.name)

    def read_remote_ifaces(self):
        """read remote interfaces config"""
        ifaces = ""
        if self.active:
            command = "/sbin/ifconfig | /bin/grep -i addr "
            ifaces, errors = self._read_remote_command(command)
            if len(errors):
                print "Error:", errors
                return
            self._write_local_file("interfaces", ifaces)
            self.adm6git.add(self.path)
            self.adm6git.commit("interface-config read from: " + self.name)

    def read_remote_routes(self):
        """read remote routing table"""
        if self.active:
            command = "/sbin/ip -6 route show"
            routen, errors = self._read_remote_command(command)
            if len(errors):
                print "Error:", errors
                return
            self._write_local_file("routes", routen)
            self.adm6git.add(self.path)
            self.adm6git.commit("routes read from: " + self.name)


if __name__ == "__main__":
    CFG = Adm6ConfigParser()
    GIT = Adm6Git()
    GIT.add(GIT.home)
    if not GIT.old:
        GIT.commit("Initial very first commit")
    DEVS = CFG.get_devices()
    PKEY_FILE = '/home/hans/.ssh/id_rsa'
    KEY = paramiko.RSAKey.from_private_key_file(PKEY_FILE)
    SESSION = paramiko.SSHClient()
    SESSION.load_system_host_keys()
    #s.set_log_channel('p.log')
    #port = 22
    #username = 'root'
    for host_name in DEVS.split(','):
        This = RemoteHost(host_name)
        This.read_remote_ifaces()
        This.read_remote_routes()
        This.apply_newest_output()
