#! /usr/bin/env python
#coding=utf-8
""" Module provides object for git repository within adm6:

    Intentnion of this module :
    1. if not exists adm_home/.git, create and init a git repository
       create .gitignore as well, f.e.
    2. every apply do the appropriate, check in all the config changes
    3. produce a human readable logfile of all activities,
       only first 1000 Entries or the like
"""

import os
import subprocess
import sys
from adm6configparser import Adm6ConfigParser


class Adm6Git():
    """ adm6Git implements stuff to handle a git repository
        located in adm6 homedirectory"""

    def __init__(self):
        self.cfg = Adm6ConfigParser()
        self.home = self.cfg.get_adm6_home()
        self.pwd = os.getcwd()
        os.chdir(self.home)
        if os.path.exists(self.home + ".git"):
            self.old = True
            os.chdir(self.pwd)
            return
        else:
            print self.home + ".git  does not exist"
            print "creating git repository"
            self.old = False
        cmd = "/usr/bin/git init"
        mes = subprocess.Popen(cmd, shell=True,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, )
        msgs = mes.communicate()
        for tup in msgs:
            if "empty Git" in tup:
                break
            else:
                print "something went wrong: ", msgs
        os.chdir(self.pwd)

    def add(self, new_path):
        """ return output of: git add new_path"""
        os.chdir(self.home)
        cmd = "/usr/bin/git add " + new_path
        mes = subprocess.Popen(cmd, shell=True,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, )
        msgs = mes.communicate()
        print_head(msgs)
        os.chdir(self.pwd)

    def commit(self, message):
        """ return output of: git add new_path"""
        if len(message) == 0 or message == None:
            print "try to commit without message, abort"
            sys.exit(1)
        os.chdir(self.home)
        cmd = "/usr/bin/git commit -a -m'" + message + "'"
        mes = subprocess.Popen(cmd, shell=True,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, )
        msgs = mes.communicate()
        print_head(msgs)
        os.chdir(self.pwd)

    def log(self, lines=15):
        """show output of: git log"""
        os.chdir(self.home)
        cmd = "/usr/bin/git log"
        mes = subprocess.Popen(cmd, shell=True,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, )
        msgs = mes.communicate()
        print_head(msgs, lines)
        os.chdir(self.pwd)

    def status(self):
        """ return output of: git status"""
        os.chdir(self.home)
        #cmd = "/usr/bin/git status"
        #ret = subprocess.call(cmd, shell=True)
        cmd = "/usr/bin/git status"
        mes = subprocess.Popen(cmd, shell=True,
                stdout=subprocess.PIPE, stderr=subprocess.PIPE, )
        msgs = mes.communicate()
        print_head(msgs)
        os.chdir(self.pwd)


def print_head(message, max_lines=10):
    """print first max lines of tupel"""
    cnt = 0
    #print "#"*80
    for tup in message[0].split('\n'):
        cnt += 1
        if cnt <= max_lines:
            print tup

if __name__ == "__main__":
    #"""adm6Git main test program"""
    AGIT = Adm6Git()
    if not AGIT.old:
        AGIT.add("etc")
        AGIT.add("desc")
        MES = "Initial commit\n"
        MSH = AGIT.home
        MES = MES + "Content:\n%setc/*\n%sdesc/*\n" % (MSH, MSH)
        AGIT.commit(MES)
        AGIT.status()
    print "pwd: ", os.getcwd()
    print "old:", AGIT.old
    AGIT.log()
    #print dir(AGIT)
